const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productController = require("../controllers/productController");

// Add Product
router.post("/addProducts", (req, res) => {
  productController.addProduct(req.body).then((result) => res.send(result));
});

// Retreiving all Product
router.get("/all", (req, res) => {
  productController.getAllProduct().then((result) => res.send(result));
});

// Retreive all Active
router.get("/active", (req, res) => {
  productController.getAllActive().then((result) => res.send(result));
});
// Retreive all archive
router.get("/archive", (req, res) => {
  productController.getAllArchive().then((result) => res.send(result));
});

// Retreiving Single Product
router.get("/:productId", (req, res) => {
  productController
    .retreiveSingleProduct(req.params)
    .then((result) => res.send(result));
});

// Update Product
router.put("/:productId", (req, res) => {
  productController
    .updateProduct(req.params, req.body)
    .then((result) => res.send(result));
});

// retreive Product with quantity
router.get("/:productId/", (req, res) => {
  productController.productInfo(req.params).then((result) => res.send(result));
});

// Archive Product
router.put("/:productId/archive", (req, res) => {
  productController
    .archiveProduct(req.params)
    .then((result) => res.send(result));
});

// Archive Product
router.put("/:productId/activate", (req, res) => {
  productController
    .activeProduct(req.params)
    .then((result) => res.send(result));
});

module.exports = router;
