const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController");

// register user
router.post("/register", (req, res) => {
  userController.registerUser(req.body).then((result) => res.send(result));
});

// Register Admin
router.post("/register-admin", (req, res) => {
  userController.registerAdmin(req.body).then((result) => res.send(result));
});

// Retreiving all User
router.get("/allUser", auth.verify, (req, res) => {
  let data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };

  userController.getAllUser(data).then((result) => res.send(result));
});

// login the User
router.post("/login", (req, res) => {
  userController.LoginUser(req.body).then((result) => res.send(result));
});

// Users checkout
router.post("/checkout", auth.verify, (req, res) => {
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    productId: req.body.productId,
    productName: req.body.productName,
    quantity: req.body.quantity,
    price: req.body.price,
  };

  userController.CheckOut(data).then((result) => res.send(result));
});

// retrieving user details
router.get("/userDetails", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  userController
    .detailUser({ userId: userData.id })
    .then((result) => res.send(result));
});

// retreive admin user
router.get("/admin", (req, res) => {
  userController.getAllAdmin().then((result) => res.send(result));
});

// retreive client user
router.get("/client", (req, res) => {
  userController.getAllClient().then((result) => res.send(result));
});

module.exports = router;
