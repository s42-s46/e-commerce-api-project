const Product = require("../models/Product");

// Start Add product
module.exports.addProduct = (data) => {
  let newProduct = new Product({
    productName: data.productName,
    description: data.description,
    size: data.size,
    color: data.color,
    price: data.price,
    endUser: data.endUser,
    photos: data.photos,
  });

  return newProduct.save().then((product, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};
// End Add Prodcut

// Start Retreiving all Product
module.exports.getAllProduct = (data) => {
  return Product.find({}).then((result) => {
    return result;
  });
};

// Start Retreiving All Active Product
module.exports.getAllActive = () => {
  return Product.find({ isActive: true }).then((result) => {
    return result;
  });
};

// Start Retreiving All Active Product
module.exports.getAllArchive = () => {
  return Product.find({ isActive: false }).then((result) => {
    return result;
  });
};

// Retreiving Single Product
module.exports.retreiveSingleProduct = (reqParams) => {
  return Product.findById(reqParams.productId).then((result) => {
    return result;
  });
};

// Retreiving product and quantity
module.exports.productInfo = (reqParams) => {
  return Product.findById(reqParams.productId).then((result) => {
    return result;
  });
};

// Updating Product
module.exports.updateProduct = (reqParams, reqBody) => {
  let updateProds = {
    productName: reqBody.productName,
    description: reqBody.description,
    size: reqBody.size,
    color: reqBody.color,
    price: reqBody.price,
    endUser: reqBody.endUser,
    photos: reqBody.photos,
    isActive: reqBody.isActive,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateProds).then(
    (product, err) => {
      if (err) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// Archive Product
module.exports.archiveProduct = (reqParams) => {
  let updateActiveField = {
    isActive: false,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(
    (product, err) => {
      if (err) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// Active Product
module.exports.activeProduct = (reqParams) => {
  let updateProduct = {
    isActive: true,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then(
    (product, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};
