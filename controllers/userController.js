const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register User
module.exports.registerUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result) {
      return false;
    } else {
      let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
      });

      return newUser.save().then((user, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
    }
  });
};

// Register Admin
module.exports.registerAdmin = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result) {
      return false;
    } else {
      let adminEnrolled = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        isAdmin: "true",
      });

      return adminEnrolled.save().then((user, err) => {
        if (err) {
          return false;
        } else {
          return true;
        }
      });
    }
  });
};
// End Create New User

// Login User
module.exports.LoginUser = (reqbody) => {
  return User.findOne({ email: reqbody.email }).then((result) => {
    if (result == null || !result) {
      return false;
    } else {
      const isPasswordisCorrect = bcrypt.compareSync(
        reqbody.password,
        result.password
      );

      if (isPasswordisCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};
// end Login

// Non-User CheckOut
module.exports.CheckOut = async (data) => {
  if (data.isAdmin === true) {
    return false;
  } else {
    let isUserUpdate = await User.findById(data.userId).then((user) => {
      user.ordereredProduct.push({
        products: {
          productId: data.productId,
          productName: data.productName,
          quantity: data.quantity,
          price: data.price,
        },

        totalAmount: data.quantity * data.price,
      });

      return user.save().then((user, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
    });

    let isProductUpdate = await Product.findById(data.productId).then(
      (product) => {
        product.userOrders.push({ userId: data.userId });

        return product.save().then((product, err) => {
          if (err) {
            return false;
          } else {
            return true;
          }
        });
      }
    );

    if (isUserUpdate && isProductUpdate) {
      return true;
    } else {
      return false;
    }
  }
};

// retreiving User details
module.exports.detailUser = (data) => {
  return User.findById(data.userId).then((result) => {
    result.password = " ";
    return result;
  });
};

// retreiving User details
module.exports.getAllUser = (data) => {
  return User.findById(data.id).then((result) => {
    return result;
  });
};

// get all admin user
module.exports.getAllAdmin = () => {
  return User.find({ isAdmin: true }).then((result) => {
    result.password = " ";
    return result;
  });
};

// get all client user
module.exports.getAllClient = () => {
  return User.find({ isAdmin: false }).then((result) => {
    result.password = " ";
    return result;
  });
};
