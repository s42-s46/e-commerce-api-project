const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();
const port = 4001;

// connecting to mongoDB
mongoose.connect(
  "mongodb+srv://joram_182:joramape182@zuitt-bootcamp.kq3szvv.mongodb.net/Capstone-2?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.once("open", () => console.log("Now connected to mongoDB"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Main URI
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(port, () => console.log(`API is now online on port ${port}`));
