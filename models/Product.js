const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
  productName: {
    type: String,
    required: [true, "Product name is required"],
  },

  description: {
    type: String,
    required: [true, "Description is required"],
  },

  size: {
    type: String,
    required: [true, "Size is required"],
  },

  color: {
    type: String,
    required: [true, "Color is required"],
  },

  price: {
    type: Number,
    required: [true, "Price is required"],
  },

  endUser: {
    type: String,
    required: [true, "End User is required"],
  },

  photos: {
    type: String,
    required: [true, "Photos is required"],
  },

  isActive: {
    type: Boolean,
    default: false,
  },

  createdOn: {
    type: Date,
    default: new Date(),
  },

  userOrders: [
    {
      userId: {
        type: String,
        required: [true, "user ID is required"],
      },
    },
  ],
});

module.exports = mongoose.model("Product", ProductSchema);
