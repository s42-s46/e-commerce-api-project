const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name is required!"],
  },

  lastName: {
    type: String,
    required: [true, "Last name is required!"],
  },

  email: {
    type: String,
    required: [true, "Password is required"],
  },

  isAdmin: {
    type: Boolean,
    default: false,
  },

  password: {
    type: String,
    required: [true, "Password is required"],
  },

  ordereredProduct: [
    {
      products: [
        {
          productId: {
            type: String,
            required: [true, "Product ID id required"],
          },

          productName: {
            type: String,
            required: [true, "Product name is required"],
          },
          quantity: {
            type: Number,
            required: [true, "Quantity is required"],
          },

          price: {
            type: Number,
            required: [true, "Quantity is required"],
          },
        },
      ],

      totalAmount: {
        type: Number,
        required: [true, "Total Amount is required"],
      },

      purchasedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
